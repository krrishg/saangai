package com.saangai.handler;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.security.Key;

public class TokenHandler {
	
	private static Key key;

	public static String generateToken(String username) {
		key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
		String token = Jwts.builder().setSubject(username).signWith(key).compact();
		return token;
	}

	public static boolean verifyToken(String token, String username) {
		try {
			if (Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().getSubject().equals(username) && !token.equals("")){
				return true;
			} else {
				return false;
			}
		} catch (Exception exception) {
			return false;
		}
	}

	public static void destroyToken() {
		//jws = null;
	}
}
