package com.saangai.handler.connection;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.ResultSet;
import org.mindrot.jbcrypt.BCrypt;
import com.saangai.handler.connection.ConnectionHandler;
import com.saangai.handler.TokenHandler;

public class UserConnection {
	private static Connection connection = ConnectionHandler.getConnection();
	private static Statement statement = ConnectionHandler.getStatement();

	public static String insertIntoUser(String username, String password, String email) {
		String successMessage = "";
		String errorMessage = ConnectionHandler.connectToDatabase();

		if (errorMessage.equals("true")) {
			try {
				String insertionQuery = "INSERT INTO social.USER VALUES('" + username + "', '" + password + "', '" + email + "')";
				statement.executeUpdate(insertionQuery);
				connection.commit();
				connection.close();
				successMessage = "Registration is completed. You can now login to your account.";
			} catch (SQLException exception) {
				String userSelectionQuery = "SELECT * FROM social.USER WHERE username='" + username + "'";

				try {
					ConnectionHandler.connectToDatabase();
					ResultSet resultSet = statement.executeQuery(userSelectionQuery);
					connection.commit();
					connection.close();
					if (resultSet.next()) {
						successMessage = "User " + username + " already exists. Please retry with another username.";	
					} else {
						String emailSelectionQuery = "SELECT * FROM social.USER WHERE email='" + email + "'";
						ConnectionHandler.connectToDatabase();
						resultSet = statement.executeQuery(emailSelectionQuery);
						connection.commit();
						connection.close();
						if (resultSet.next()) {
							successMessage = "Email address is already in use. Please try with another email address.";	
						}
					}
				} catch (SQLException sqlException) {
					successMessage = "Unexpected error occured. Please try again later, or let us know about this.";
				}
			}
			return successMessage;
		} else {
			return errorMessage;
		}
	}

	public static String selectFromUser(String username, String password) {
		String successMessage = "";
		String errorMessage = ConnectionHandler.connectToDatabase();
		
		if (errorMessage.equals("true")) {
			try {
				String userSelectionQuery = "SELECT * FROM social.USER WHERE username='" + username + "'";
				ResultSet resultSet = statement.executeQuery(userSelectionQuery);
				connection.commit();
				connection.close();
				if (resultSet.next()) {
					String hashedPassword = resultSet.getString("password");
					if (BCrypt.checkpw(password, hashedPassword)) {
						String token = TokenHandler.generateToken(username);
						successMessage = "Token:" + token;
					} else {
						successMessage = "Username or password did not match. Please try again with correct username and password.";
					}
				} else {
					successMessage = "Username or password did not match. Please try again with correct username and password.";
				}
			} catch (SQLException sqlException) {
				successMessage = "Unexpected error occured. Please try again later, or let us know about this.";
			}
			return successMessage;
		} else {
			return errorMessage;
		}
	}
}
