package com.saangai.handler.connection;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.saangai.handler.XMLHandler;

public class ConnectionHandler {
	private static Connection connection;
	private static Statement statement;

	public static Connection getConnection() {
		return connection;
	}

	public static Statement getStatement() {
		return statement;
	}

	public static String connectToDatabase() {
		try {
			Class.forName("org.postgresql.Driver");	
			connection = DriverManager.getConnection(XMLHandler.getDriver() + "://" + XMLHandler.getHost() + ":" + XMLHandler.getPort() + "/" + XMLHandler.getDatabaseName(), XMLHandler.getDatabaseUser(), XMLHandler.getDatabasePassword());


			connection.setAutoCommit(false);	
			statement = connection.createStatement();
			return "true";
		} catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException exception) {
			return "Error connecting to server. Please check your internet connection.";
		}
	}
}
