package com.saangai.handler.connection;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.saangai.handler.connection.ConnectionHandler;

public class PostConnection {

	private static Connection connection = ConnectionHandler.getConnection();
	private static Statement statement = ConnectionHandler.getStatement();
	
	public static String insertIntoPost(String username, String post) {
		String successMessage = "";
		String errorMessage = ConnectionHandler.connectToDatabase();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		String currentDateTime = dateFormat.format(calendar.getTime());

		if (errorMessage.equals("true")) {
			try {
				String insertionQuery = "INSERT INTO social.POST VALUES('" + username + "','" + post + "', '" + currentDateTime + "')";
				statement.executeUpdate(insertionQuery);
				connection.commit();
				connection.close();
				successMessage = "Post updated successfully.";
			} catch (SQLException exception) {
				successMessage = "Unexpected error occured. Please try again later, or let us know about this.";
			}
			return successMessage;
		} else {
			return errorMessage;
		}
	}

	public static String deleteFromPost(int postId) {
		String successMessage = "";
		String errorMessage = ConnectionHandler.connectToDatabase();
		
		if (errorMessage.equals("true")) {
			try {
				String deletionQuery = "DELETE FROM post where post_id=" + postId;
				statement.executeUpdate(deletionQuery);
				connection.commit();
				connection.close();
				successMessage = "Post deleted successfully.";
			} catch (SQLException exception) {
				successMessage = "Unexpected error occured. Please try again later, or let us know about this.";
			}
			return successMessage;
		} else {
			return errorMessage;
		}
	}
}
