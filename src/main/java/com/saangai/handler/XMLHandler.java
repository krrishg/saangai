package com.saangai.handler;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.io.File;

public class XMLHandler {
	private static File configFile;
	private static DocumentBuilderFactory builderFactory;
	private static DocumentBuilder documentBuilder;
	private static Document document;
	private static NodeList nodeList;
	private static Node rootNode;
	private static Element rootElement;

	private static void setDatabaseProperties() throws ParserConfigurationException, SAXException, IOException {
		configFile = new File("db.properties");
		builderFactory = DocumentBuilderFactory.newInstance();
		documentBuilder = builderFactory.newDocumentBuilder();
		document = documentBuilder.parse(configFile);
		document.getDocumentElement().normalize();
		nodeList = document.getElementsByTagName("database");
		rootNode = nodeList.item(0);
		rootElement = (Element) rootNode;
	}
	
	public static String getDriver() throws ParserConfigurationException, SAXException, IOException {
		setDatabaseProperties();
		return rootElement.getElementsByTagName("driver").item(0).getTextContent();
	}

	public static String getHost() throws ParserConfigurationException, SAXException, IOException {
		setDatabaseProperties();
		return rootElement.getElementsByTagName("host").item(0).getTextContent();
	}

	public static String getPort() throws ParserConfigurationException, SAXException, IOException {
		setDatabaseProperties();
		return rootElement.getElementsByTagName("port").item(0).getTextContent();
	}

	public static String getDatabaseName() throws ParserConfigurationException, SAXException, IOException {
		setDatabaseProperties();
		return rootElement.getElementsByTagName("name").item(0).getTextContent();
	}

	public static String getDatabaseUser() throws ParserConfigurationException, SAXException, IOException {
		setDatabaseProperties();
		return rootElement.getElementsByTagName("user").item(0).getTextContent();	
	}

	public static String getDatabasePassword() throws ParserConfigurationException, SAXException, IOException {
		setDatabaseProperties();
		return rootElement.getElementsByTagName("password").item(0).getTextContent();	
	}
}
