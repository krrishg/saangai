package com.saangai.service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface FriendService {
	@WebMethod String addFriend(String friend, String username, String token);
	@WebMethod String unfriend(String friend, String username, String token);
}
