package com.saangai.service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface UserService {
	@WebMethod String registerUser(String username, String password, String email);
	@WebMethod String loginUser(String username, String password);
	@WebMethod String logoutUser();
}
