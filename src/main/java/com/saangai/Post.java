package com.saangai;

import com.saangai.service.PostService;
import com.saangai.handler.PostConnection;
import com.saangai.handler.TokenHandler;
import javax.jws.WebService;

@WebService(endpointInterface = "com.saangai.service.PostService")
public class Post implements PostService {
	
	@Override
	public String createPost(String post, String username, String token) {
		if (TokenHandler.verifyToken(token, username)) {
			return PostConnection.insertIntoPost(username, post);
		}
		return "Invalid token. Please login and try again.";
	}

	@Override
	public String deletePost(int postId, String username, String token) {
		if (TokenHandler.verifyToken(token, username)) {
			return PostConnection.deleteFromPost(postId);
		}
		return "Invalid token. Please login and try again.";
	}

	@Override
	public String retrieveAllPosts() {
		
		return "";
	}
}
