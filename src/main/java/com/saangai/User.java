package com.saangai;

import com.saangai.service.UserService;
import javax.jws.WebService;
import com.saangai.handler.UserConnection;
import com.saangai.handler.TokenHandler;
import org.mindrot.jbcrypt.*;
import org.apache.commons.validator.routines.EmailValidator;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.regex.Pattern;

@WebService(endpointInterface = "com.saangai.service.UserService")
public class User implements UserService {

	@Override
	public String registerUser(String username, String password, String email) {
		String validationMessage = validate(username, password, email);
		if (validationMessage.equals("Valid")) {
			password = BCrypt.hashpw(password, BCrypt.gensalt());
			return UserConnection.insertIntoUser(username, password, email);
		} else {
			return validationMessage;
		}
	}

	@Override
	public String loginUser(String username, String password) {
		return UserConnection.selectFromUser(username, password);
	}

	@Override
	public String logoutUser() {
		TokenHandler.destroyToken();
		return "Logged out successfully.";
	}

	private static String validate(String username, String password, String email) {
		if (username.trim().equals("") || password.equals("") || email.trim().equals("")) {
			return "Field(s) cannot be empty."; 
		} else if (Pattern.matches(username,  "\\b[a-zA-Z][a-zA-Z0-9\\-._]{3,}\\b")) {
			return "Invalid username."; 
		} else if (password.length() < 8) {
			return "Password must be atleast 8 characters long.";
		} else if (!EmailValidator.getInstance(true).isValid(email)) {
			return "Invalid e-mail address.";
		} else {
			return "Valid";
		}
	}
}
