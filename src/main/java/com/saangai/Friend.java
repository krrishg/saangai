package com.saangai;

import com.saangai.service.FriendService;
import com.saangai.handler.FriendConnection;
import com.saangai.handler.TokenHandler;
import javax.jws.WebService;

@WebService(endpointInterface = "com.saangai.service.FriendService")
public class Friend implements FriendService {

	private static String errorMessage = "Invalid token. Please login and try again.";
	
	@Override
	public String addFriend(String friend, String username, String token) {
		if (TokenHandler.verifyToken(token, username)) {
			return FriendConnection.insertIntoFriend(username, friend);
		}
		return errorMessage;
	}

	@Override
	public String unfriend(String friend, String username, String token) {
		if (TokenHandler.verifyToken(token, username)) {
			return FriendConnection.deleteFromFriend(username, friend);
		}
		return errorMessage;
	}
}
