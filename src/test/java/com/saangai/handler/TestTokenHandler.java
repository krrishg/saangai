package com.saangai.handler;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class TestTokenHandler {
	
	@Test
	public void testVerifyToken() {
		String username = "abcdefg";
		String token = TokenHandler.generateToken(username);

		// valid token
		assertTrue(TokenHandler.verifyToken(token, username));
	
		// invalid token
		username = "johndoe";
		assertFalse(TokenHandler.verifyToken(token, username));
	}
}
